const imageContainer = document.getElementById('image-container');

// Boîte à propos
const divApropos = document.getElementById('div-a-propos');
const darkbox = document.getElementById('darkbox');

function ouvre(div){
  div.classList.remove('hide');
  darkbox.classList.remove('hide');
}


function ferme(div){
  div.classList.add('hide');
  darkbox.classList.add('hide');
}

if (localStorage.getItem('appel-images')) {
  console.log("liste d'image à restaurer");
  let listeImagesStockage = JSON.parse(localStorage.getItem('appel-images'));
  
  const imageContainer = document.getElementById('image-container');

  for (let i = 0; i < listeImagesStockage.length; i++) {
    const storedImage = listeImagesStockage[i];
    const image = document.createElement('img');
    image.src = storedImage.data;
    image.addEventListener('click', () => {
      image.classList.toggle('selected');
    });
    imageContainer.appendChild(image);
  }
}



function displayImages(event) {
  console.log(event.target.files);
  const files = event.target.files;
  const fileListArray = [];

  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    if (file.type.startsWith('image/')) {
      const image = document.createElement('img');
      image.src = URL.createObjectURL(file);
      image.addEventListener('click', () => {
        image.classList.toggle('selected');
      });
      imageContainer.appendChild(image);
      
      // Read file as DataURL and store in localStorage
      const reader = new FileReader();
      reader.onload = function(e) {
        fileListArray.push({ name: file.name, data: e.target.result });
        localStorage.setItem('appel-images', JSON.stringify(fileListArray));
      };
      reader.readAsDataURL(file);
    }
  }
}

const folderInput = document.getElementById('folder-input');
folderInput.addEventListener('change', displayImages);

const presentButton = document.getElementById('presentButton');
const absentButton = document.getElementById('absentButton');

presentButton.addEventListener('click', () => {
  const selectedImages = document.querySelectorAll('#image-container img.selected');
  selectedImages.forEach((image) => {
    const presentImagesContainer = document.getElementById('presentImagesContainer');
    presentImagesContainer.appendChild(image);
    image.classList.remove('selected');
  });
});

absentButton.addEventListener('click', () => {
  const selectedImages = document.querySelectorAll('#image-container img.selected');
  selectedImages.forEach((image) => {
    const absentImagesContainer = document.getElementById('absentImagesContainer');
    absentImagesContainer.appendChild(image);
    image.classList.remove('selected');
  });
});


function raz() {
  console.log("raz");
  
  const imagesPresents = presentImagesContainer.querySelectorAll('img');
  const imagesAbsents = absentImagesContainer.querySelectorAll('img');
  
  imagesPresents.forEach(image => {
    imageContainer.appendChild(image);
  });

  imagesAbsents.forEach(image => {
    imageContainer.appendChild(image);
  });

}

function vide() {
  console.log("vide");  
  folderInput.value='';
  imageContainer.innerHTML='';
  localStorage.removeItem("appel-images");
  raz();
}